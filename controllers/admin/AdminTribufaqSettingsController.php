<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminTribufaqSettingsController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $token = Tools::getAdminTokenLite('AdminModules');
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=tribufaq&token=' . $token);
    }
}