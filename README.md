# aznoqmous/tribufaq

Salut la tribu !

Une fois le module installé sur prestashop, et afin de tester rapidement le projet je vous invite à utiliser le fichier ``tribufaq.sql`` à disposition dans ce repo !  

Vous pourrez l'importer simplement en vous plaçant dans le dossier du module via la commande suivante :
````shell
mysql -u <user> -p <database_name> < tribufaq.sql
````

## TODO BONUS
- [x] Add tinymce to Question
- [x] Display Category title on Question backend list
- [x] Prevent unactive categories from being displayed
