-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: localhost    Database: prestashop-17811
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ps_tribufaq_question`
--

DROP TABLE IF EXISTS `ps_tribufaq_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tribufaq_question` (
  `id_tribufaq_question` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tribufaq_category` int(10) unsigned NOT NULL,
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id_tribufaq_question`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tribufaq_question`
--

LOCK TABLES `ps_tribufaq_question` WRITE;
/*!40000 ALTER TABLE `ps_tribufaq_question` DISABLE KEYS */;
INSERT INTO `ps_tribufaq_question` VALUES (1,1,'2024-06-11 19:39:29',1),(2,2,'2024-06-11 19:39:42',1),(3,1,'2024-06-11 19:39:53',1),(4,1,'2024-06-11 19:40:12',0),(5,1,'2024-06-11 20:35:40',1);
/*!40000 ALTER TABLE `ps_tribufaq_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tribufaq_question_lang`
--

DROP TABLE IF EXISTS `ps_tribufaq_question_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tribufaq_question_lang` (
  `id_tribufaq_question` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(5) unsigned NOT NULL,
  `question` varchar(255) NOT NULL,
  `response` text NOT NULL,
  PRIMARY KEY (`id_tribufaq_question`,`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tribufaq_question_lang`
--

LOCK TABLES `ps_tribufaq_question_lang` WRITE;
/*!40000 ALTER TABLE `ps_tribufaq_question_lang` DISABLE KEYS */;
INSERT INTO `ps_tribufaq_question_lang` VALUES (1,1,'Bonjour ?','<p>C\'est pas une <strong>question<br /><br /></strong><strong></strong></p>\r\n<ul>\r\n<li><strong>adazdazd</strong></li>\r\n<li><strong>azdazdaz</strong></li>\r\n<li><strong>azdaz</strong></li>\r\n<li><strong>dazda</strong></li>\r\n<li><strong>zdazd</strong></li>\r\n</ul>'),(2,1,'Quoi ?','Je n\'en reviens pas'),(3,1,'Comment ?','Qu\'ouïe je'),(4,1,'Qu\'entends-je','azdazdazdazd'),(5,1,'Test test','<p>Super test</p>');
/*!40000 ALTER TABLE `ps_tribufaq_question_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tribufaq_category`
--

DROP TABLE IF EXISTS `ps_tribufaq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tribufaq_category` (
  `id_tribufaq_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id_tribufaq_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tribufaq_category`
--

LOCK TABLES `ps_tribufaq_category` WRITE;
/*!40000 ALTER TABLE `ps_tribufaq_category` DISABLE KEYS */;
INSERT INTO `ps_tribufaq_category` VALUES (1,'2024-06-11 19:39:07',1),(2,'2024-06-11 19:39:16',1);
/*!40000 ALTER TABLE `ps_tribufaq_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tribufaq_category_lang`
--

DROP TABLE IF EXISTS `ps_tribufaq_category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tribufaq_category_lang` (
  `id_tribufaq_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tribufaq_category`,`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tribufaq_category_lang`
--

LOCK TABLES `ps_tribufaq_category_lang` WRITE;
/*!40000 ALTER TABLE `ps_tribufaq_category_lang` DISABLE KEYS */;
INSERT INTO `ps_tribufaq_category_lang` VALUES (1,1,'Catégorie 1'),(2,1,'Catégorie 2');
/*!40000 ALTER TABLE `ps_tribufaq_category_lang` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-11 19:43:41
