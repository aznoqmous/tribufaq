<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . '/tribufaq/classes/ModuleClassUtility.php';
include_once _PS_MODULE_DIR_ . '/tribufaq/src/Entity/TribufaqQuestion.php';
include_once _PS_MODULE_DIR_ . '/tribufaq/src/Entity/TribufaqCategory.php';
include_once _PS_MODULE_DIR_ . '/tribufaq/controllers/admin/AdminTribufaqSettingsController.php';

class TribuFaq extends Module
{
    protected $queries = [];
    protected $moduleTabs = [];

    public $errors = [];
    protected $updated = false;

    public function __construct()
    {
        $this->name = 'tribufaq';
        $this->version = '1.0';
        $this->author = 'Tribu and Co';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Tribu FAQ');
        $this->description = $this->l('Affiche une FAQ catégorisée sur la page d\'accueil');

        $this->moduleTabs = [
            [
                'name' => $this->l('Gestion FAQ'),
                'class_name' => 'AdminParentTribufaq',
                'parent_class_name' => 'TRIBU',
                'icon' => 'help_outline'
            ],
            [
                'name' => $this->l('Catégories FAQ'),
                'class_name' => 'AdminTribufaqCategory',
                'parent_class_name' => 'AdminParentTribufaq',
            ],
            [
                'name' => $this->l('Questions/réponses'),
                'class_name' => 'AdminTribufaqQuestion',
                'parent_class_name' => 'AdminParentTribufaq',
            ],
            [
                'name' => $this->l('Paramètres'),
                'class_name' => 'AdminTribufaqSettings',
                'parent_class_name' => 'AdminParentTribufaq',
            ]
        ];
        $this->queries = [
            'tribufaq_question' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'tribufaq_question` (
                `id_tribufaq_question` INT(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_tribufaq_category` INT(10) unsigned NOT NULL,
                `date_add` DATETIME DEFAULT CURRENT_TIMESTAMP,
                `active` int(1) unsigned DEFAULT "0",
                PRIMARY KEY (`id_tribufaq_question`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;',
            'tribufaq_question_lang' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'tribufaq_question_lang` (
                `id_tribufaq_question` INT(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_lang` int(5) unsigned NOT NULL,
                `question` VARCHAR(255) NOT NULL,
                `response` text NOT NULL,
                PRIMARY KEY (`id_tribufaq_question`,`id_lang`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;',
            'tribufaq_category' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'tribufaq_category` (
                `id_tribufaq_category` INT(10) unsigned NOT NULL AUTO_INCREMENT,
                `date_add` DATETIME DEFAULT CURRENT_TIMESTAMP,
                `active` int(1) unsigned DEFAULT "0",
                PRIMARY KEY (`id_tribufaq_category`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;',
            'tribufaq_category_lang' => 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'tribufaq_category_lang` (
                `id_tribufaq_category` INT(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_lang` int(5) unsigned NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id_tribufaq_category`,`id_lang`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;'
        ];
    }

    public function install()
    {
        return (
            parent::install()
            && $this->registerHook('displayHome')
            && $this->registerHook('actionFrontControllerSetMedia')
            && $this->registerHook('actionAdminControllerSetMedia')
            && ModuleClassUtility::installSql($this->queries)
            && ModuleClassUtility::installModuleTabs($this->name, $this->moduleTabs)
        );
    }

    public function uninstall()
    {
        return (
            parent::uninstall()
            && ModuleClassUtility::removeTabByClassName('AdminTribufaqCategory')
            && ModuleClassUtility::removeTabByClassName('AdminTribufaqQuestion')
            && ModuleClassUtility::removeTabByClassName('AdminParentTribufaq')
            && ModuleClassUtility::uninstallsql($this->queries)
        );
    }

    public function hookActionFrontControllerSetMedia()
    {
        $this->context->controller->registerStylesheet(
            'tribufaq-style',
            $this->_path . 'views/css/tribufaq.css',
            [
                'media' => 'all',
                'priority' => 1000,
            ]
        );

        $this->context->controller->registerJavascript(
            'tribufaq-javascript',
            $this->_path . 'views/js/tribufaq.js',
            [
                'position' => 'bottom',
                'priority' => 1000,
            ]
        );
    }

    public function hookDisplayHome()
    {
        $count = Configuration::get('tribufaq_count');

        // Récupère les X questions les plus récentes
        $questions = TribufaqQuestion::getLastFaq($count);

        // façonne le tableau "faqs" en se basant sur les questions les plus récentes
        // on obtient alors un tableau de catégories les plus récentes
        $faqs = [];
        foreach ($questions as $question) {
            // on initialise la catégorie
            if (!isset($faqs[$question['id_tribufaq_category']])) $faqs[$question['id_tribufaq_category']] = [
                'category_name' => TribufaqCategory::getCategoryName($question['id_tribufaq_category']),
                'questions' => []
            ];
            // on push la question dans la catégorie
            $faqs[$question['id_tribufaq_category']]['questions'][] = $question;
        }

        $this->context->smarty->assign([
            'title' => Configuration::get('tribufaq_title'),
            'faqs' => $faqs
        ]);

        return $this->display(__FILE__, 'displayHome.tpl');
    }

    /**
     * Add backend styles
     * @return void
     */
    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/tribufaq-backend.css');
    }

    /**
     * Méthode pour afficher la configuration du module
     * @return string Le contenu HTML de la page
     */
    public function getContent()
    {
        $output = '';

        $this->checkValue("tribufaq_title");
        $this->checkValue("tribufaq_count");

        if (count($this->errors)) $output = implode("", $this->errors);
        else if ($this->updated) $output = $this->displayConfirmation($this->l('Modifications enregistrées !'));

        return $output . $this->displayForm();
    }

    private function cleanErrors()
    {
        $this->updated = false;
        $this->errors = [];
    }

    public function checkValue($fieldName)
    {
        $error = null;

        if (Tools::isSubmit('submit' . $this->name)) {
            // retrieve the value set by the user
            $configValue = (string)Tools::getValue($fieldName);

            // check that the value is valid
            if (empty($configValue) || !Validate::isGenericName($configValue)) {
                // invalid value, show an error
                $this->errors[] = $this->displayError($this->l("Le Champ \"$fieldName\" est requis !"));
            } else {
                // value is ok, update it and display a confirmation message
                $this->updated = true;
                Configuration::updateValue($fieldName, $configValue);
            }
        }

        return $error;
    }

    public function displayForm()
    {
        // Init Fields form array
        $count = Configuration::get("tribufaq_count");

        $form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Configuration'),
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('Titre du module'),
                        'name' => 'tribufaq_title',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'html',
                        'label' => $this->l('Nombre de question à afficher'),
                        'required' => true,
                        'name' => 'tribufaq_count',
                        'html_content' => "<input type=\"number\" name=\"tribufaq_count\" value=\"$count\">"
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                ],
            ],
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->table = $this->table;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&' . http_build_query(['configure' => $this->name]);
        $helper->submit_action = 'submit' . $this->name;

        // Default language
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');

        // Load current value into the form
        $helper->fields_value['tribufaq_title'] = Tools::getValue('tribufaq_title', Configuration::get('tribufaq_title'));
        $helper->fields_value['tribufaq_name'] = Tools::getValue('tribufaq_name', Configuration::get('tribufaq_name'));

        return $helper->generateForm([$form]);
    }

}