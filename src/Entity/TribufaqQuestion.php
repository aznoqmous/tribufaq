<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
class TribufaqQuestion extends ObjectModel
{
    public $id_tribufaq_question;
    public $id_tribufaq_category;
    public $question;
    public $response;
    public $active;
    public $date_add;

    public static $definition = [
        'table'     => 'tribufaq_question',
        'primary'   => 'id_tribufaq_question',
        'multilang' => true,
        'fields'    => [
            'id_tribufaq_category' => ['type' => self::TYPE_INT, 'required' => true],
            'question'             => ['type' => self::TYPE_STRING, 'required' => true, 'lang' => true, 'validate' => 'isGenericName'],
            'response'             => ['type' => self::TYPE_HTML, 'required' => true, 'lang' => true, 'validate' => 'isCleanHtml'],
            'active'               => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'active' => 'status'],
            'date_add'             => ['type' => self::TYPE_DATE, 'required' => false]
        ],
    ];

    public static function getLastFaq($number)
    {
        $query = new DbQuery();
        $query->from('tribufaq_question', 'faq');
        $query->select('faq.id_tribufaq_category, faq.date_add, faq.active, question, response');
        $query->leftJoin('tribufaq_question_lang', 'faql', 'faq.id_tribufaq_question = faql.id_tribufaq_question AND faql.id_lang=' . Context::getContext()->language->id);
        $query->leftJoin('tribufaq_category', 'faqc', 'faq.id_tribufaq_category = faqc.id_tribufaq_category');
        $query->where('faq.active = 1');
        $query->where('faqc.active = 1');
        $query->orderBy('faq.date_add DESC');
        if($number >= 0) $query->limit((int)$number); // prevent LIMIT -1 error
        return Db::getInstance()->executeS($query);
    }
}